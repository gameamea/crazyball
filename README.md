# Description
jeu de type casse brique

# Objectifs
Détruire toutes les briques (sauf les incassables, elles sont gris foncé) pour passer au niveau suivant.

Le jeu ne comprend que 4 niveaux.

# Déplacements
Déplacer le paddle : mouvement de la souris
Quitter le jeu : clic sur la croix ou alt+F4 ou revenir à l'écran de démarrage et cliquer sur quitter.

# Interactions
En heurtant la balle avec les cotés du paddle on peut envoyer la balle dans une direction choisie.
La vitesse de la balle varie constamment en fonction des mouvement verticaux de la souris, du hasard et du point de contact avec le paddle. 
Le jeu est perdu si la balle atteint le bas de l'écran

# Copyrights
Créé avec unity3D version 4.7

Tiré de la formation "The Complete Unity Developer" de Udemy, section 5

# Téléchargement et aperçu 
Vous pouvez télécharger l'exécutable, les sources et tester le jeu sur la [page CrazyBall de GameJolt](
http://gamejolt.com/dashboard/games/153965).

------------------
(C) 2016 Steamlead (www.steamlead.com)