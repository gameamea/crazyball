﻿using UnityEngine;

public class Ball : MonoBehaviour {
	// en changeant ces valeurs on modifie la difficulté
	[Tooltip("vitesse de base de la balle en Y")]
	public float
		baseBallYSpeed = 10f;
	[Tooltip("activer pour ajouter une force à la balle en fonction de la vitesse de déplacement de la souris")]
	public bool
		useMouseAcceleration;// NMP
	[Tooltip("plus cette valeur est importante plus l'impact de la vitesse de la souris est important")]
	public float
		mouseVelocityMultiplier = 1.1f;
	[Tooltip("limite minimum de valeur aléatoire ajoutée à la vitesse de la balle en X à chaque rebond")]
	public float
		randomVelocityOnBounceMinX = -0.3f;// une valeur négative permet de ralentir la balle et donc de faciliter le jeu
	[Tooltip("limite maximum de valeur aléatoire ajoutée à la vitesse de la balle en X à chaque rebond")]
	public float
		randomVelocityOnBounceMaxX = 0.3f;
	[Tooltip("limite minimum de valeur aléatoire ajoutée à la vitesse de la balle en Y à chaque rebond")]
	public float
		randomVelocityOnBounceMinY = -0.3f;// une valeur négative permet de ralentir la balle et donc de faciliter le jeu
	[Tooltip("limite maximum de valeur aléatoire ajoutée à la vitesse de la balle en Y à chaque rebond")]
	public float
		randomVelocityOnBounceMaxY = 0.1f;
	[Tooltip("plus cette valeur est grande, plus les effets de rebonds aléatoires sont importants")]
	public int
		randomVelocityMultiplier = 150;
	[Tooltip(" plus cette valeur est grande, plus les effets de la zone de rebond de la balle sur le paddle sont importants")]
	public int
		paddleDeltaVelocityMultiplier = 150;
	[Tooltip("son par défaut pour les contacts de la balle")]
	public AudioClip
		ballSound;
	Paddle paddle;
	Vector3 paddleToBallvector;
	Vector3 mouseDelta = Vector3.zero;
	Vector3 lastMousePosition = Vector3.zero;
	float mouseAcceleration;
	bool hasStarted = false;
	float ballRotationZ = 2.0f;
	SpriteRenderer spriteRenderer;// permet de gerer la couleur de la balle
	
	void Start () { 
		paddle = GameObject.FindObjectOfType<Paddle> ();
		paddleToBallvector = paddle.transform.position - transform.position;
		spriteRenderer = GetComponent<SpriteRenderer> ();
		rigidbody2D.isKinematic = true;// pour éviter que la balle ne bouge avant son lancement
	}

	void FixedUpdate () {
		// on calcule l'acceleration verticale de la souris 
		mouseDelta = Input.mousePosition - lastMousePosition;
		if (useMouseAcceleration && mouseDelta.y > 0) {
			mouseAcceleration = mouseDelta.y * mouseVelocityMultiplier;
			Debug.Log ("mouseAcceleration " + mouseAcceleration);	
		}
		if (hasStarted) {
			// on ajoute une rotation de la balle
			transform.Rotate (new Vector3 (0.0f, 0.0f, ballRotationZ));
		}
		lastMousePosition = Input.mousePosition;
	}

	void Update () {
		if (!hasStarted) {
			// on fige la position de la balle respectivement à celle du paddle
			transform.position = paddle.transform.position - paddleToBallvector;
			// on attend un clic gauche pour lancer la balle
			if (Input.GetMouseButtonDown (0)) {
				hasStarted = true;
				rigidbody2D.velocity = new Vector2 (2.0f, baseBallYSpeed);
				rigidbody2D.isKinematic = false;
			}
		}
	}

	void OnCollisionEnter2D (Collision2D collision) {
		if (collision == null) {
			throw new System.ArgumentNullException ("collider");
		}
		Vector2 tweakVelocity = Vector2.zero, paddleVelocity = Vector2.zero, mouseVelocity = Vector2.zero;
		float alfa=1f;
		if (hasStarted) {
			//Debug.Log ("Enter " + this.name + ":" + MethodBase.GetCurrentMethod ().Name);

			//Debug.Log ("collider " + collider.gameObject.name);
			switch (collision.gameObject.tag) {
			case "Paddle":
				// en cas de contact avec le paddle
				// on calcule la différence de position en X entre le centre de la balle et celui du paddle 
				float Xdiff = transform.position.x - collision.gameObject.transform.position.x;
				//Debug.Log ("Xdiff:" + Xdiff);
				// on utilise cette valeur pour modifier la velocité de la balle
				// par défaut on détermine une velocité aléatoire sur l'axe des Y et une basée sur Xdiff sur l'axe des X
				paddleVelocity = new Vector2 (Xdiff * paddleDeltaVelocityMultiplier, 0.0f);
				
				// gestion de l'accélération due à la souris
				// TODO : ne fonctionne pas correctement car tient compte uniquement du mouvement de la souris et pas de sa vitesse de déplacement
				if (mouseAcceleration > 0) {
					Debug.Log ("Adding acceleration:" + mouseAcceleration);
					mouseVelocity = new Vector2 (0, mouseAcceleration);
					alfa=.4f; // balle semi transparente
					
				} else {
					alfa=1f; // balle semi transparente
					
				}
				// on joue le son de la balle par défaut
				if (ballSound) {
					AudioSource.PlayClipAtPoint (ballSound, transform.position);
				}
				break;
			case "Breakable":
				// par défaut on détermine une velocité aléatoire
				tweakVelocity = new Vector2 (Random.Range (randomVelocityOnBounceMinX, randomVelocityOnBounceMaxX), Random.Range (randomVelocityOnBounceMinY, randomVelocityOnBounceMaxY)) * randomVelocityMultiplier;
				break;		
				
			// dans le cas d'un objet "Breakable", le son est joué par l'objet lui-meme 
			default:	
				// par défaut on détermine une velocité aléatoire
				tweakVelocity = new Vector2 (Random.Range (randomVelocityOnBounceMinX, randomVelocityOnBounceMaxX), Random.Range (randomVelocityOnBounceMinY, randomVelocityOnBounceMaxY)) * randomVelocityMultiplier;
				// on joue le son de la balle par défaut
				if (ballSound) {
					AudioSource.PlayClipAtPoint (ballSound, transform.position);
				}
				break;					
			}
			// on ajoute de la velocité pour ajouter un peu de hasard dans les rebonds ou tenir compte de la position de la balle sur la paddle
			Vector2 globalVelocity = tweakVelocity + paddleVelocity + mouseVelocity;
			if (globalVelocity != Vector2.zero) {
				//Debug.Log ("ajout tweakVelocity:" + tweakVelocity);
				rigidbody2D.AddForce (globalVelocity);
				if (globalVelocity.x > 0 && globalVelocity.y > 0) {
					spriteRenderer.color = new Color (1f, 0.5f, 0.5f, alfa); // balle couleur rouge
				} else  if (globalVelocity.x > 0 || globalVelocity.y > 0) {
					spriteRenderer.color = new Color (1f, 1f, 0.5f, alfa); // balle couleur jaune
				} else  if (globalVelocity.x < 0 && globalVelocity.y < 0) {
					spriteRenderer.color = new Color (0.5f, 0.5f, 1f, alfa); // balle couleur bleue
				} else  if (globalVelocity.x < 0 || globalVelocity.y < 0) {
					spriteRenderer.color = new Color (0.5f, 1f, 0.5f, alfa); // balle couleur verte
				}
			} else {
				spriteRenderer.color = new Color (1.0f, 1.0f, 1.0f, alfa); // balle couleur blanche (normale)			
			}

			
		}
	}
}
