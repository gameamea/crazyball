﻿using UnityEngine;

public class LevelManager : MonoBehaviour {
	[Tooltip("valeur de base de la gravité")]
	public float
		gravitySettingsBase = -1.0f;
	[Tooltip("son lorsque le joueur perd la partie")]
	public AudioClip
		nextLevelSound;
	int loadedLevelIndex;
	MusicPlayer musicPlayer;

	void Start () {
		InitLevel ();	
	}

	public void LoadLevel (string levelName) {
		Debug.Log ("Level " + levelName + " loaded"); 
		if (nextLevelSound) {			
			AudioSource.PlayClipAtPoint (nextLevelSound, transform.position);
			//TODO : le son est interrompu par le chargement du niveau suivant
		}
		
		Application.LoadLevel (levelName);
	}

	public void LoadNextLevel () {
		loadedLevelIndex = Application.loadedLevel + 1;
		if (loadedLevelIndex > Application.levelCount - 4) {
			// on a chargé tous les niveau existant (on deduit les 4 scenes qui ne sont pas des niveaux jouables)
			// on a gagné
			Application.LoadLevel ("Win Screen");
			
		} else {
			Debug.Log ("Level n°" + loadedLevelIndex + " loaded"); 
			Application.LoadLevel (loadedLevelIndex);
		}
	}

	public void QuitRequest () {
		Debug.Log ("Quit application");
		Application.Quit ();
			
	}

	public void BrickDestroyed () {
		if (Brick.breakableCount <= 0) {
			Debug.Log ("All bricks destroyed !");
			LoadNextLevel ();
		}
	}

	void InitLevel () {
		musicPlayer = GameObject.FindObjectOfType<MusicPlayer> ();
		
		loadedLevelIndex = Application.loadedLevel + 1;
		
		// on modifie les propriété des niveaux jouables uniquement
		if (loadedLevelIndex >= 1 && loadedLevelIndex < Application.levelCount - 4) {
		
			float newDifficulty = loadedLevelIndex / 3.0f;
			Debug.Log ("Difficulty set to:" + newDifficulty);
		
			// cette ligne permet de changer la gravité et donc la vitesse de la balle et donc la difficulté du jeu
			Physics.gravity = new Vector3 (0.0f, gravitySettingsBase - newDifficulty, 0.0f);
		
			// on change les propriété de la balle en fonction du niveau
			Ball ball;
			ball = GameObject.FindObjectOfType<Ball> ();
			if (ball) {
				ball.useMouseAcceleration = true;// on active l'acceleration de la souris, c'est plus drole
				ball.mouseVelocityMultiplier = 1.1f + newDifficulty / 10;
				ball.randomVelocityOnBounceMinX = -0.2f + newDifficulty / 5.0f;
				ball.randomVelocityOnBounceMaxX = 0.3f + newDifficulty / 2.0f;
				ball.randomVelocityOnBounceMinY = -0.2f + newDifficulty / 5.0f;
				ball.randomVelocityOnBounceMaxY = 0.1f + newDifficulty / 2.0f;
				ball.randomVelocityMultiplier = 80 + (int)(newDifficulty * 20.0f);
				ball.paddleDeltaVelocityMultiplier = 100 + (int)(newDifficulty * 20.0f);
			} else {
				Debug.Log ("Missing a Ball in this level !");
			}
		} 
	}
}
