﻿using UnityEngine;

public class Brick : MonoBehaviour {
	[Tooltip("Sprites à utiliser pour les différents états de la brique")]
	public Sprite[]
		hitSprites;
	[Tooltip("son lorsque la brique est touchée")]
	public AudioClip
		touchSound;
	[Tooltip("son lorsque la brique est détruite")]
	public AudioClip
		destroySound;
	[Tooltip("activer pour que la brique soit destructible")]
	public bool
		isBreakable;
	[Tooltip("effet utilisé lors de la destruction de la brique")]
	public GameObject
		smoke;
	public static int breakableCount = 0;
	int maxHits;
	int timesHit;
	LevelManager levelManager;
	SpriteRenderer spriteRenderer;
	
	void Start () {
		timesHit = 0;
		isBreakable = (tag == "Breakable");
		levelManager = GameObject.FindObjectOfType<LevelManager> ();
		if (isBreakable) {
			breakableCount++;
		}
		spriteRenderer = GetComponent<SpriteRenderer> ();
		spriteRenderer.sprite = hitSprites [0];// on fixe l'image de base au premier sprite du tableau
		maxHits = hitSprites.Length;// différent du code initial car j'ai ajouté l'élement 0hit dans le tableau hitSprites des prefabs de briques
	}

	void OnCollisionEnter2D (Collision2D collision) {
		if (collision == null) {
			throw new System.ArgumentNullException ("collision");
		}
		if (isBreakable) {
			HandleHits ();
		}
	}

	void HandleHits () {
		timesHit++;
		
		//Debug.log ("ball touch brick " + timesHit);
		if (timesHit >= maxHits) {
			if (destroySound) {
				AudioSource.PlayClipAtPoint (destroySound, transform.position);
			}
			DestroyEffect ();
			Destroy (gameObject);
			breakableCount--;
			// verifie si toutes les briques ont été détruites
			levelManager.BrickDestroyed ();
		} else {
			if (touchSound) {
				AudioSource.PlayClipAtPoint (touchSound, transform.position);
			}
			LoadSprites ();
		}
	}

	void DestroyEffect () {
		Vector3 smokePosition = gameObject.transform.position;
		smokePosition.y -= 0.5f;// 
		GameObject smokeInstance = (GameObject)Instantiate (smoke, smokePosition, Quaternion.identity);
		if (smokeInstance) {
			smokeInstance.GetComponent<ParticleSystem> ().startColor = spriteRenderer.color;
		}
	}

	void LoadSprites () {
		int spriteIndex = timesHit;// différent du code initial car j'ai ajouté l'élement 0hit dans le tableau hitSprites des prefabs de briques
		//Debug.Log ("Sprite changed to " + spriteIndex);
		if (hitSprites [spriteIndex]) {
			GetComponent<SpriteRenderer> ().sprite = hitSprites [spriteIndex];
		} else {
			Debug.LogError ("Sprite n°" + spriteIndex + " is not defined");
		}
	}
}
